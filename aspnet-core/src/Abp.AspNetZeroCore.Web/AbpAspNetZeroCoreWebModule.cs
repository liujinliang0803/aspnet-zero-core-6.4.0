﻿using System;
using Abp.AspNetCore;
using Abp.Modules;

namespace Abp.AspNetZeroCore.Web
{
	// Token: 0x02000002 RID: 2
	[DependsOn(new Type[]
	{
		typeof(AbpAspNetZeroCoreModule)
	})]
	[DependsOn(new Type[]
	{
		typeof(AbpAspNetCoreModule)
	})]
	public class AbpAspNetZeroCoreWebModule : AbpModule
	{

		public override void PreInitialize()
		{
		}


		public override void Initialize()
		{
			base.IocManager.RegisterAssemblyByConvention(typeof(AbpAspNetZeroCoreWebModule).Assembly);
		}
	}
}
