﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x02000006 RID: 6
	public class ExternalAuthManager : IExternalAuthManager, ITransientDependency
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000020EF File Offset: 0x000002EF
		public ExternalAuthManager(IIocResolver iocResolver, IExternalAuthConfiguration externalAuthConfiguration)
		{
			this._iocResolver = iocResolver;
			this._externalAuthConfiguration = externalAuthConfiguration;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002108 File Offset: 0x00000308
		public Task<bool> IsValidUser(string provider, string providerKey, string providerAccessCode)
		{
			Task<bool> result;
			using (IDisposableDependencyObjectWrapper<IExternalAuthProviderApi> disposableDependencyObjectWrapper = this.CreateProviderApi(provider))
			{
				result = disposableDependencyObjectWrapper.Object.IsValidUser(providerKey, providerAccessCode);
			}
			return result;
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002148 File Offset: 0x00000348
		public Task<ExternalAuthUserInfo> GetUserInfo(string provider, string accessCode)
		{
			Task<ExternalAuthUserInfo> userInfo;
			using (IDisposableDependencyObjectWrapper<IExternalAuthProviderApi> disposableDependencyObjectWrapper = this.CreateProviderApi(provider))
			{
				userInfo = disposableDependencyObjectWrapper.Object.GetUserInfo(accessCode);
			}
			return userInfo;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002188 File Offset: 0x00000388
		public IDisposableDependencyObjectWrapper<IExternalAuthProviderApi> CreateProviderApi(string provider)
		{
			ExternalLoginProviderInfo externalLoginProviderInfo = this._externalAuthConfiguration.Providers.FirstOrDefault((ExternalLoginProviderInfo p) => p.Name == provider);
			if (externalLoginProviderInfo == null)
			{
				throw new Exception("Unknown external auth provider: " + provider);
			}
			IDisposableDependencyObjectWrapper<IExternalAuthProviderApi> disposableDependencyObjectWrapper = IocResolverExtensions.ResolveAsDisposable<IExternalAuthProviderApi>(this._iocResolver, externalLoginProviderInfo.ProviderApiType);
			disposableDependencyObjectWrapper.Object.Initialize(externalLoginProviderInfo);
			return disposableDependencyObjectWrapper;
		}

		// Token: 0x04000003 RID: 3
		private readonly IIocResolver _iocResolver;

		// Token: 0x04000004 RID: 4
		private readonly IExternalAuthConfiguration _externalAuthConfiguration;
	}
}
