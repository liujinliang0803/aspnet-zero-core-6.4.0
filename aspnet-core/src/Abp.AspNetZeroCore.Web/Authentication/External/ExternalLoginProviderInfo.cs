﻿using System;
using System.Collections.Generic;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x02000009 RID: 9
	public class ExternalLoginProviderInfo
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001E RID: 30 RVA: 0x000022CA File Offset: 0x000004CA
		// (set) Token: 0x0600001F RID: 31 RVA: 0x000022D2 File Offset: 0x000004D2
		public string Name { get; set; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000020 RID: 32 RVA: 0x000022DB File Offset: 0x000004DB
		// (set) Token: 0x06000021 RID: 33 RVA: 0x000022E3 File Offset: 0x000004E3
		public string ClientId { get; set; }

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000022 RID: 34 RVA: 0x000022EC File Offset: 0x000004EC
		// (set) Token: 0x06000023 RID: 35 RVA: 0x000022F4 File Offset: 0x000004F4
		public string ClientSecret { get; set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000024 RID: 36 RVA: 0x000022FD File Offset: 0x000004FD
		// (set) Token: 0x06000025 RID: 37 RVA: 0x00002305 File Offset: 0x00000505
		public Type ProviderApiType { get; set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000026 RID: 38 RVA: 0x0000230E File Offset: 0x0000050E
		// (set) Token: 0x06000027 RID: 39 RVA: 0x00002316 File Offset: 0x00000516
		public Dictionary<string, string> AdditionalParams { get; set; }

		// Token: 0x06000028 RID: 40 RVA: 0x0000231F File Offset: 0x0000051F
		public ExternalLoginProviderInfo(string name, string clientId, string clientSecret, Type providerApiType, Dictionary<string, string> additionalParams = null)
		{
			this.Name = name;
			this.ClientId = clientId;
			this.ClientSecret = clientSecret;
			this.ProviderApiType = providerApiType;
			this.AdditionalParams = additionalParams;
		}
	}
}
