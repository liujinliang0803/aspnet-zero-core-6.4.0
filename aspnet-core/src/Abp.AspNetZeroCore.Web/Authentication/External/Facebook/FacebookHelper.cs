﻿using System;
using Newtonsoft.Json.Linq;

namespace Abp.AspNetZeroCore.Web.Authentication.External.Facebook
{
	// Token: 0x02000013 RID: 19
	public static class FacebookHelper
	{
		// Token: 0x06000047 RID: 71 RVA: 0x00002770 File Offset: 0x00000970
		public static string GetId(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("id");
		}

		// Token: 0x06000048 RID: 72 RVA: 0x0000278B File Offset: 0x0000098B
		public static string GetAgeRangeMin(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return FacebookHelper.TryGetValue(user, "age_range", "min");
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000027AB File Offset: 0x000009AB
		public static string GetAgeRangeMax(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return FacebookHelper.TryGetValue(user, "age_range", "max");
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000027CB File Offset: 0x000009CB
		public static string GetBirthday(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("birthday");
		}

		// Token: 0x0600004B RID: 75 RVA: 0x000027E6 File Offset: 0x000009E6
		public static string GetEmail(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("email");
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002801 File Offset: 0x00000A01
		public static string GetFirstName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("first_name");
		}

		// Token: 0x0600004D RID: 77 RVA: 0x0000281C File Offset: 0x00000A1C
		public static string GetGender(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("gender");
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002837 File Offset: 0x00000A37
		public static string GetLastName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("last_name");
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002852 File Offset: 0x00000A52
		public static string GetLink(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("link");
		}

		// Token: 0x06000050 RID: 80 RVA: 0x0000286D File Offset: 0x00000A6D
		public static string GetLocation(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return FacebookHelper.TryGetValue(user, "location", "name");
		}

		// Token: 0x06000051 RID: 81 RVA: 0x0000288D File Offset: 0x00000A8D
		public static string GetLocale(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("locale");
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000028A8 File Offset: 0x00000AA8
		public static string GetMiddleName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("middle_name");
		}

		// Token: 0x06000053 RID: 83 RVA: 0x000028C3 File Offset: 0x00000AC3
		public static string GetName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("name");
		}

		// Token: 0x06000054 RID: 84 RVA: 0x000028DE File Offset: 0x00000ADE
		public static string GetTimeZone(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("timezone");
		}

		// Token: 0x06000055 RID: 85 RVA: 0x000028FC File Offset: 0x00000AFC
		private static string TryGetValue(JObject user, string propertyName, string subProperty)
		{
			if (user.TryGetValue(propertyName, out var jtoken))
			{
				JObject jobject = JObject.Parse(jtoken.ToString());
				if (jobject != null && jobject.TryGetValue(subProperty, out jtoken))
				{
					return jtoken.ToString();
				}
			}
			return null;
		}
	}
}
