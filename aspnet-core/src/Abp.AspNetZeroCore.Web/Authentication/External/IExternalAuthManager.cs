﻿using System;
using System.Threading.Tasks;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x0200000B RID: 11
	public interface IExternalAuthManager
	{
		// Token: 0x0600002A RID: 42
		Task<bool> IsValidUser(string provider, string providerKey, string providerAccessCode);

		// Token: 0x0600002B RID: 43
		Task<ExternalAuthUserInfo> GetUserInfo(string provider, string accessCode);
	}
}
