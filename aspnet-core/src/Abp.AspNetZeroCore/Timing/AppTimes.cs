﻿using System;
using System.Runtime.CompilerServices;
using Abp.Dependency;

namespace Abp.AspNetZeroCore.Timing
{
	public class AppTimes : ISingletonDependency
	{
		public DateTime StartupTime { get; set; }=  DateTime.Now;
	}
}
