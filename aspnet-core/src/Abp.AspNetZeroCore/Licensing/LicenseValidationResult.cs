﻿using System;
using System.Runtime.CompilerServices;

namespace Abp.AspNetZeroCore.Licensing
{

    public class LicenseValidationResult
    {

        public bool Success { get; set; } = true;


        public bool LastRequest { get; set; } = true;


        public string ControlCode { get; set; } = "auper";


        public LicenseValidationResult()
        {
            Console.WriteLine("LicenseValidationResult");
        }
    }
}
