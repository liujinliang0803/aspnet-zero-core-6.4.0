﻿using System;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Abp.AspNetZeroCore.V;
using Abp.Dependency;
using Abp.Threading;
using Abp.Zero.Configuration;
using Castle.Core.Logging;

namespace Abp.AspNetZeroCore.Licensing
{
	public sealed class AspNetZeroLicenseChecker : AspNetZeroBaseLicenseChecker, ISingletonDependency
	{
	
		public ILogger Logger { get; set; }

		public AspNetZeroLicenseChecker(AspNetZeroConfiguration configuration = null, IAbpZeroConfig abpZeroConfig = null, string configFilePath = "") : base(configuration, abpZeroConfig, configFilePath)
		{
			this.Logger = NullLogger.Instance;
		}

		public void Check()
		{
//			if (base.IsThereAReasonToNotCheck())
//			{
//				return;
//			}
		}

		public void CheckSync()
		{
			try
			{
				AsyncHelper.RunSync(new Func<Task>(this.CheckInternal));
			}
			catch (AspNetZeroLicenseException ex)
			{
                Console.WriteLine(ex.Message);
                Environment.Exit(-42);
            }
			catch (Exception ex2)
			{
                Console.WriteLine(ex2.Message);
			}
		}


		private Task CheckInternal()
		{
            Console.WriteLine("CheckInternal");
			return Task.FromResult(true);
		}

		private bool CheckedBefore()
        {
            return true;
        }

		private bool IsProjectNameValid()
		{
			return base.CompareProjectName(this.GetHashedProjectName());
		}

		private string GetHashedProjectName()
        {
            return string.Empty;
        }

		private Task ValidateLicenseOnServer()
		{
			return Task.FromResult(true);
		}

		private string GetLicenseCodeWithoutProjectNameHash()
        {
            return string.Empty;
        }

		private Task<LicenseValidationResult> ValidateLicense(string licenseCode)
        {
            Console.WriteLine($"licenseCode={licenseCode}");
            return Task.FromResult(new LicenseValidationResult()
            {
                ControlCode = "auper",
                LastRequest = true,
                Success = true
            });
        }


		private void UpdateLastLicenseCheckDate()
		{
            File.WriteAllText(this._licenseCheckFilePath, this.GetHashedValue(GetTodayAsString()));
		}

		private void MarkAsLastRequest()
        {
            var content = this.GetHashedValue(AspNetZeroLicenseChecker.GetLicenseExpiredString());
            Console.WriteLine($"MarkAsLastRequest，this._licenseCheckFilePath={this._licenseCheckFilePath},hashed:{content}");
            File.WriteAllText(this._licenseCheckFilePath, content);
		}


		private string GetLastLicenseCheckDate()
        {
            return File.ReadAllText(this._licenseCheckFilePath);
		}

		private static string GetUniqueComputerId()
		{
			return (from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.OperationalStatus == OperationalStatus.Up
			select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
		}

	
		private static string GetComputerName()
		{
			return Environment.MachineName;
		}


		private static string GetTodayAsString()
		{
            Console.WriteLine($"GetTodayAsString");
			return DateTime.Now.ToString("yyyy-MM-dd");
		}

		private string GetHashedValue(string str)
		{
			MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
			string result;
			try
			{
				result =EncodeBase64(md5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(str + _uniqueComputerId + GetSalt())));
			}
			finally
			{
                md5CryptoServiceProvider?.Dispose();

            }
			return result;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002B38 File Offset: 0x00000F38
		[MethodImpl(MethodImplOptions.NoInlining)]
		protected override string GetSalt()
        {
            Console.WriteLine("GetSalt");
            int[] array = new int[20];
            RuntimeHelpers.InitializeArray(array, PrivateImplementationDetails.staticArrayInitTypeSize1.GetType().GetField("staticArrayInitTypeSize1").FieldHandle);
			return StringGeneratorFromInteger(array);
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002B58 File Offset: 0x00000F58
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static string GetLicenseExpiredString()
		{
            Console.WriteLine("GetLicenseExpiredString");
			int[] array = new int[29];
            RuntimeHelpers.InitializeArray(array, PrivateImplementationDetails.staticArrayInitTypeSize2.GetType().GetField("staticArrayInitTypeSize2").FieldHandle);
            return StringGeneratorFromInteger(array);
        }

		// Token: 0x0600004D RID: 77 RVA: 0x00002B78 File Offset: 0x00000F78
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static string StringGeneratorFromInteger(int[] letters)
        {
            return letters.Aggregate(string.Empty, (t1, t2) => t1 + t2);
        }

		// Token: 0x0600004E RID: 78 RVA: 0x00002F6C File Offset: 0x0000136C
		[MethodImpl(MethodImplOptions.NoInlining)]
		protected override string GetHashedValueWithoutUniqueComputerId(string str)
		{
			MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
			string result;
			try
			{
				result = EncodeBase64(md5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(str+GetSalt())));
			}
			finally
			{
                md5CryptoServiceProvider?.Dispose();

            }
			return result;
		}

		private static string EncodeBase64(byte[] ba)
        {
            Console.WriteLine("EncodeBase64");
            return Convert.ToBase64String(ba);
        }

		private string _licenseCheckFilePath;

		private string _uniqueComputerId;
	}
}
