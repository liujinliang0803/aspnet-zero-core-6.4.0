﻿using System;
using System.Runtime.CompilerServices;

namespace Abp.AspNetZeroCore.Licensing
{
	public class LicenseCheckInfo
    {
        public string LicenseCode { get; set; } = "auper";

		public string UniqueComputerId {  get;  set; } = "auper";


        public string ProjectAssemblyName {  get;  set; } = "auper";


        public string LicenseController {  get;  set; } = "auper";


        public string ComputerName {  get;  set; } = "auper";


        public string ControlCode {  get;  set; } = "auper";


        public DateTime DateOfClient {  get;  set; }=DateTime.MaxValue;

		
		public LicenseCheckInfo()
		{
			Console.WriteLine("LicenseCheckInfo");
		}
	}
}
