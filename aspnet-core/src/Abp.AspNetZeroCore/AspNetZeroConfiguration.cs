﻿using System;
using System.Runtime.CompilerServices;

namespace Abp.AspNetZeroCore
{
	public class AspNetZeroConfiguration
	{
		public string LicenseCode {get; set; }
	}
}
