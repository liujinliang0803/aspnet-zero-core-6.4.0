﻿using System;
using System.Runtime.CompilerServices;
using Abp.Configuration.Startup;

namespace Abp.AspNetZeroCore
{
	public static class AspNetZeroConfigurationExtensions
	{
		public static AspNetZeroConfiguration AspNetZero(this IModuleConfigurations modules)
		{
			return AspNetZeroConfigurationExtensions.GetConfig(modules).Get<AspNetZeroConfiguration>();
		}

		internal static IAbpStartupConfiguration GetConfig(IModuleConfigurations moduleConfigurations)
        {
            return moduleConfigurations.AbpConfiguration;
		}
	}
}
