﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using Abp.AspNetZeroCore.Licensing;
using Abp.Dependency;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Abp.AspNetZeroCore
{
	public class AbpAspNetZeroCoreModule : AbpModule
	{
		public override void PreInitialize()
        {
            IocManager.Register<AspNetZeroConfiguration>(DependencyLifeStyle.Singleton);
		}

		public override void Initialize()
		{
            IocManager.RegisterAssemblyByConvention(typeof(AbpAspNetZeroCoreModule).GetAssembly());
		}

		public override void PostInitialize()
		{
			var disposableDependencyObjectWrapper = IocManager.ResolveAsDisposable<AspNetZeroLicenseChecker>();
			try
			{
                disposableDependencyObjectWrapper.Object.Check();
            }
			finally
			{
                disposableDependencyObjectWrapper?.Dispose();
            }
		}
	}
}
